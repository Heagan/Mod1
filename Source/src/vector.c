#include "../includes/mod.h"

int				isVecEqual(t_vector a, t_vector b) {
	if (a.x != b.x || a.y != b.y || a.z != b.z || a.fx != b.fx || a.fy != b.fy) {
		return (0);
	}
	return (1);
}

t_vector		mkvec(int x, int y, int z)
{
	t_vector	t;

	t.x = x;
	t.y = y;
	t.z = z;
	return (t);
}

t_point		mkpoint2(int p1, int p2, int p3, int p4)
{
	t_point	t;

	t.p1 = p1;
	t.p2 = p2;
	t.p3 = p3;
	t.p4 = p4;
	return (t);
}