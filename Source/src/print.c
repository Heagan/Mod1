#include "../includes/mod.h"

void		print_2d_int_array(int array[WORLDY][WORLDX])
{
	int		x;
	int		y;

	y = -1;
	while (++y < WORLDY)
	{
		x = -1;
		while (++x < WORLDX)
		{
			ft_putnbr(array[y][x]);
			ft_putchar(' ');
		}
		ft_putchar('\n');
	}
}