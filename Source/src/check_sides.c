#include "../includes/mod.h"

int		check_below(t_main *a, t_vector t)
{
	if (!(t.y < WORLDY - 1 && 
		(!a->f.v[t.z][t.y + 1][t.x] &&
			!a->f.v2[t.z][t.y + 1][t.x] &&
				!a->f.t[t.z][t.y + 1][t.x])))
					return (0);
	a->f.v2[t.z][t.y + 1][t.x]++;
	a->f.v[t.z][t.y][t.x]--;
	return (1);
}

int		check_above(t_main *a, t_vector t)
{
	if (t.y > 0 && t.y < WORLDY)
	if (a->f.v[t.z][t.y - 1][t.x] || a->f.v[t.z][t.y][t.x] > 1)
	{
		a->f.v2[t.z][t.y - 1][t.x]++;
		a->f.v[t.z][t.y][t.x]--;
		return (1);
	}
	return (0);
}

int		side_clear_x(t_main *a, t_vector t, int side)
{
	if (side)
		if (t.x < WORLDX && t.y < WORLDY && t.y > 0)
			if (!a->f.t[t.z][t.y][t.x + 1])
				if (!a->f.t[t.z][t.y + 1][t.x + 1])
					if (!a->f.t[t.z][t.y - 1][t.x + 1])
						return (1);
	if (!side)
		if (t.x > 0 && t.y < WORLDY && t.y > 0)
			if (!a->f.t[t.z][t.y][t.x - 1])
				if (!a->f.t[t.z][t.y + 1][t.x - 1])
					if (!a->f.t[t.z][t.y - 1][t.x - 1])
						return (1);
	return (0);
}

int		side_clear_z(t_main *a, t_vector t, int side)
{
	if (side)
		if (t.z < WORLDX && t.y < WORLDY && t.y > 0)
			if (!a->f.t[t.z + 1][t.y][t.x])
				if (!a->f.t[t.z + 1][t.y + 1][t.x])
					if (!a->f.t[t.z + 1][t.y - 1][t.x])
						return (1);
	if (!side)
		if (t.z > 0 && t.y < WORLDY && t.y > 0)
			if (!a->f.t[t.z - 1][t.y][t.x])
				if (!a->f.t[t.z - 1][t.y + 1][t.x])
					if (!a->f.t[t.z - 1][t.y - 1][t.x])
						return (1);
	return (0);
}

int		check_side_x(t_main *a, t_vector t)
{
	static int	n;

	n = ft_tog(n);
	if (n)
		return (0);
	if (t.x < WORLDX - 1 && (!a->f.v[t.z][t.y][t.x + 1] && !a->f.v2[t.z][t.y][t.x + 1] && !a->f.t[t.z][t.y][t.x + 1]))
	{
		a->f.v2[t.z][t.y][t.x + 1]++;
		a->f.v[t.z][t.y][t.x]--;
		return (1);
	}
	if (t.x > 1 && (!a->f.v[t.z][t.y][t.x - 1] && !a->f.v2[t.z][t.y][t.x - 1] && !a->f.t[t.z][t.y][t.x - 1]))
	{
		a->f.v2[t.z][t.y][t.x - 1]++;
		a->f.v[t.z][t.y][t.x]--;
	
		return (1);
	}
	return (0);
}

int		check_side_z(t_main *a, t_vector t)
{
	static int	n;

	n = ft_tog(n);
	if (!n)
		return (0);
	if (t.z < WORLDZ - 1 && (!a->f.v[t.z + 1][t.y][t.x] && !a->f.v2[t.z + 1][t.y][t.x] && !a->f.t[t.z + 1][t.y][t.x]))
	{
		a->f.v2[t.z + 1][t.y][t.x]++;
		a->f.v[t.z][t.y][t.x]--;
	
		return (1);
	}
	if (t.z > 1 && (!a->f.v[t.z - 1][t.y][t.x] && !a->f.v2[t.z - 1][t.y][t.x] && !a->f.t[t.z - 1][t.y][t.x]))
	{
		a->f.v2[t.z - 1][t.y][t.x]++;
		a->f.v[t.z][t.y][t.x]--;
	
		return (1);
	}
	return (0);
}

int		check_diag_x(t_main *a, t_vector t)
{
	static int	n;

	n = ft_tog(n);
	if (n)
		return (0);
	if (t.x < WORLDX - 1 && t.y < WORLDY - 1 &&
		side_clear_x(a, mkvec(t.x - 1 , t.y, t.z), 1) &&
		(!a->f.v[t.z][t.y + 1][t.x + 1] && !a->f.v2[t.z][t.y + 1][t.x + 1] && !a->f.t[t.z][t.y + 1][t.x + 1]))
	{
		a->f.v2[t.z][t.y + 1][t.x + 1]++;
		a->f.v[t.z][t.y][t.x]--;
		return (0);
	}
	if (t.x > 1 && t.y < WORLDY - 1 &&
		side_clear_x(a, mkvec(t.x + 1 , t.y, t.z), 0) &&
		(!a->f.v[t.z][t.y + 1][t.x - 1] && !a->f.v2[t.z][t.y + 1][t.x - 1] && !a->f.t[t.z][t.y + 1][t.x - 1]))
	{
		a->f.v2[t.z][t.y + 1][t.x - 1]++;
		a->f.v[t.z][t.y][t.x]--;
		return (1);
	}
	return (0);
}

int		check_diag_z(t_main *a, t_vector t)
{
	static int	n;

	n = ft_tog(n);
	if (!n)
		return (0);
	if (t.z > 1 && t.y < WORLDY - 1 &&
		side_clear_z(a, mkvec(t.x , t.y, t.z + 1), 0) &&
		(!a->f.v[t.z - 1][t.y + 1][t.x] && !a->f.v2[t.z - 1][t.y + 1][t.x] && !a->f.t[t.z - 1][t.y + 1][t.x]))
	{
		a->f.v2[t.z - 1][t.y + 1][t.x]++;
		a->f.v[t.z][t.y][t.x]--;
		return (1);
	}
	if (t.z < WORLDZ - 1 && t.y < WORLDY - 1 &&
		side_clear_z(a, mkvec(t.x , t.y, t.z - 1), 1) &&
		(!a->f.v[t.z + 1][t.y + 1][t.x] && !a->f.v2[t.z + 1][t.y + 1][t.x] && !a->f.t[t.z + 1][t.y + 1][t.x]))
	{
		a->f.v2[t.z + 1][t.y + 1][t.x]++;
		a->f.v[t.z][t.y][t.x]--;
		return (0);
	}
	return (0);
}
