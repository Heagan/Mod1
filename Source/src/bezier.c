#include "../includes/mod.h"

t_vector		curve(t_main *a, t_vector p[16], float u, float v)
{
	t_vector	cu[4];
	t_vector	pu[4];
	int			i;
	
	i = -1;
	while (++i < 4)
	{
		cu[0] = p[i * 4];
		cu[1] = p[i * 4 + 1];
		cu[2] = p[i * 4 + 2];
		cu[3] = p[i * 4 + 3];
		pu[i] = calc_bezier_qua(cu, u);
	}
	return (calc_bezier_qua(pu, v));
}

t_vector		calc_bezier_qua(t_vector p[4], float t)
{
	t_vector	d;

	d.fx = (1 - t) * (1 - t) * (1 - t) * p[0].fx +
	3 * (1 - t) * (1 - t) * t * p[1].fx +
	3 * (1 - t) * t * t * p[2].fx +
	t * t * t * p[3].fx;
	
	d.fy = (1 - t) * (1 - t) * (1 - t) * p[0].fy +
	3 * (1 - t) * (1 - t) * t * p[1].fy +
	3 * (1 - t) * t * t * p[2].fy +
	t * t * t * p[3].fy;

	return (d);
}


void			surface(t_main *a, t_vector p[16])
{
	float		u;
	float		v;
	t_vector	pu;

	v = 0;
	while (v <= 1)
	{
		v += MAPQUALITY;
		u = 0;
		while (u <= 1)
		{
			u += MAPQUALITY;
			pu = curve(a, p, u, v);

	//		a->f.t2d[pu.y][pu.x] = p[(int)ft_max(15, (int)((float)(15 * (u * v))))].z;
	//		put_pixel(a, pu.fx, pu.fy, 255);
	//		printf("%f %f\n", pu.fx, pu.fy);

			if (a->s_buffer[(int)pu.fy][(int)pu.fx] == 0)
				put_pixel(a, pu.fx, pu.fy, mkcol(127 *
				(1 - ((float)p[(int)((float)(15 * (u * v)))].z / WORLDY)), 255 *
				((float)p[(int)((float)(15 * (u * v)))].z / WORLDY), 0));
		}
	}
	
}
