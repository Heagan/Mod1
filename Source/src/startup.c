#include "../includes/mod.h"

void				build_terrain(t_main *a)
{
	int				x;
	int				y;
	int				z;
	int				n;

	for (int z = 0; z < WORLDZ; z++) {
		for (int x = 0; x < WORLDX; x++) {
			y = (4 * cos(x/2)) + (4 * cos(z/2));
			y = (8 * cos(x / 4)) + (8 * cos(z / 4 ));
			//y = 0;
			//if (x > 25 && x < 35)
				y = (WORLDY + y) / 2;
			if (y >= 0 && y < WORLDY)
				a->f.t[z][y][x] = 1;
		}
	}
	for (int z = 0; z < WORLDZ; z++) {
		for (int x = 0; x < WORLDX; x++) {
			n = 0;
			for (int y = 0; y < WORLDY; y++) {
				if (a->f.t[z][y][x] > 0) {
					n = 1;
				}
				if (n) {
					a->f.t[z][y][x] = 1;
				}
			}
		}
	}
}

void				spawn_emitters(t_main *a)
{
	t_vector		t;

	
	a->f.v[0][0][0] = -1;
	a->f.v[0][0][WORLDX / 3] = -1;
	a->f.v[WORLDX / 3][0][0] = -1;
	a->f.v[0][0][WORLDX / 2] = -1;
	a->f.v[WORLDX / 2][0][0] = -1;
	
	a->f.v[WORLDX / 2 + 5][0][WORLDX / 2] = -1;
	a->f.v[WORLDX / 2][0][WORLDX / 2] = -1;
	a->f.v[WORLDX / 2 - 5][0][WORLDX / 2] = -1;


	for (int z = 0; z < WORLDZ; z++) {
		for (int y = 0; y < WORLDY; y++) {
			for (int x = 0; x < WORLDX; x++) {
				if (a->f.v[z][y][x] == -1)
					a->f.v[z][y][x] = DENSITY;
			}
		}
	}
}

void				reset(t_main *a)
{
	int				x;
	int				y;
	int				z;

	for (int z = 0; z < WORLDZ; z++) {
		for (int y = 0; y < WORLDY; y++) {
			for (int x = 0; x < WORLDX; x++) {
				a->f.v[z][y][x] = 0;
				a->f.v2[z][y][x] = 0;
				a->f.t[z][y][x] = 0;
				a->f.h[y][x] = 0;
				a->f.w2d[y][x] = 0;
				a->f.t2d[y][x] = 0;
			}
		}
	}
	
	for (int y = 0; y < SCREENY; y++) {
		for (int x = 0; x < SCREENX; x++) {
			a->s_buffer[y][x] = 0;
			a->s_terrain[y][x] = 0;
		}
	}
}
