#include "../includes/mod.h"

void		draw_line(t_main *a, t_vector p1, t_vector p2, int colour)
{
	t_line	d;

	d.dx = p2.x - p1.x;
	d.dy = p2.y - p1.y;
	d.steps = abs(d.dx) > abs(d.dy) ? abs(d.dx) : abs(d.dy);
	d.xinc = d.dx / (float)d.steps;
	d.yinc = d.dy / (float)d.steps;
	d.x = p1.x;
	d.y = p1.y;
	d.i = -1;
	while (++d.i <= d.steps)
	{
		put_pixel(a, d.x, d.y, colour);
		d.x += d.xinc;
		d.y += d.yinc;
	}
}

int		v_cross(t_vector v1, t_vector v2)
{
	return (v1.x * v2.y - v1.y * v2.x);
}

void			drawTriangle(t_main *a, t_vector v1, t_vector v2, t_vector v3, int colour)
{
	int			maxX;
	int			minX;
	int			maxY;
	int			minY;
	t_vector	q;
	t_vector	vs1;
	t_vector	vs2;
	
	maxX = ft_max(v1.x, ft_max(v2.x, v3.x));
	minX = ft_min(v1.x, ft_min(v2.x, v3.x));
	maxY = ft_max(v1.y, ft_max(v2.y, v3.y));
	minY = ft_min(v1.y, ft_min(v2.y, v3.y));
	vs1.x = v2.x - v1.x;
	vs1.y = v2.y - v1.y;
	vs2.x = v3.x - v1.x;
	vs2.y = v3.y - v1.y;

	for (int x = maxX; x <= minX; x ++)
	{
		for (int y = maxY; y <= minY; y ++)
		{
			if ((y < 0 || x < 0) || (y > SCREENY || x > SCREENX))
				return ;
			q.x = x - v1.x;
			q.y = y - v1.y;

			float s = (float)v_cross(q, vs2) / v_cross(vs1, vs2);
			float t = (float)v_cross(vs1, q) / v_cross(vs1, vs2);

			if ((s >= 0) && (t >= 0) && (s + t <= 1))
				put_pixel(a, x, y, colour);
		}
	}
}