#include "../includes/mod.h"

float		mkcol(float r, float g, float b)
{
	r = ft_clamp(0, 255, r);
	g = ft_clamp(0, 255, g);
	b = ft_clamp(0, 255, b);
	return (r * 256 * 256 + g * 256 + b);
}

float		mkcolAlpha(float a, float r, float g, float b)
{
	a = ft_clamp(0, 255, a);
	r = ft_clamp(0, 255, r);
	g = ft_clamp(0, 255, g);
	b = ft_clamp(0, 255, b);
	return (a * 255 * 255 * 255 + r * 256 * 256 + g * 256 + b);
}