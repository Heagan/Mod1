#include "../includes/mod.h"

void	refresh(t_main *a)
{
	int	x;
	int	y;
	int	z;

	for (int z = 0; z < WORLDZ; z++) {
		for (int y = 0; y < WORLDY; y++) {
			for (int x = 0; x < WORLDX; x++) {
				if (a->f.v[z][y][x] > 0)
				{
					if (!check_below(a, mkvec(x, y, z)))
						if (!check_side_x(a, mkvec(x, y, z)))
							if (!check_side_z(a, mkvec(x, y, z)))
								//if (!check_diag_x(a, mkvec(x, y, z)))
									//if (!check_diag_z(a, mkvec(x, y, z)))
										check_above(a, mkvec(x, y, z));
				}
				if (a->f.v[z][y][x]) {
					a->f.v2[z][y][x] = a->f.v[z][y][x]--;
				}
			}
		}
	}
}

void	copy_map(t_main *a)
{
	int	x;
	int	y;
	int	z;

	for (int z = 0; z < WORLDZ; z++) {
		for (int y = 0; y < WORLDY; y++) {
			for (int x = 0; x < WORLDX; x++) {
				a->f.v[z][y][x] = a->f.v2[z][y][x];
				a->f.v2[z][y][x] = 0;
			}
		}
	}
}
