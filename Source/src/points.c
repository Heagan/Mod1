#include "../includes/mod.h"

int			*rotatePoint(int x, int y)
{
	static int		cord[2];

	cord[0] = x - y;
	cord[1] =  round((x + y) / 2);
	return (cord);
}

int			*rotateScaledPoint(int length, int width, int height) {
	int		*rotatedCord;

	rotatedCord = rotatePoint(length * 10, width * 10);
	rotatedCord[0] = rotatedCord[0] + SCREENX / 2;
	rotatedCord[1] = rotatedCord[1] + height * 5;
	return (rotatedCord);
}

t_point				set_point(t_point p, int t, int d)
{
	if (p.p1 == 0)
		p.p1 = d;
	else if (p.p2 == 0)
		p.p2 = d;
	else if (p.p3 == 0)
		p.p3 = d;
	else
		p.p4 = d;
	return (p);
}

void				reset_p(t_point *p)
{
	p->p1 = 0;
	p->p2 = 0;
	p->p3 = 0;
	p->p4 = 0;
}

void				set_p(t_point *p, int n)
{
	if (p->p1 == 0)
		p->p1 = n;
	else if (p->p2 == 0)
		p->p2 = n;
	else if (p->p3 == 0)
		p->p3 = n;
	else
		p->p4 = n;
}

void				mkpoint(int h[WORLDZ][WORLDX], int xMax, int zMax, t_vector *p, t_main *a)
{
	int				*rotatedCord;
	int				i;

	i = 0;
	for (int z = zMax - 4; z < zMax; z++) {
		for (int x = xMax - 4; x < xMax; x++) {
			rotatedCord = rotateScaledPoint(x, z, h[z][x]);
			p[i].fx = rotatedCord[0];
			p[i].fy = rotatedCord[1];
			p[i].z = h[z][x];
			put_pixel(a, p[i].x, p[i].y, mkcol(255, 0, 0));
			i++;
		}
	}
}
