#include "../includes/mod.h"

t_main			a;

void				ft_error(char *s)
{
	ft_putendl(s);
	exit (-1);
}

void				startup(t_main *a)
{
	reset(a);
	build_terrain(a);
	spawn_emitters(a);
}

void				winctrl(t_main *a)
{
	int				run;

	run = 1;
	while (run)
	{
		while (SDL_PollEvent(&a->event))
		{
			if (a->event.type == SDL_QUIT)
				run = 0;
			if (a->event.type == SDL_KEYDOWN)
				if (a->event.key.keysym.sym == SDLK_ESCAPE)
					run = 0;
		}
		refresh(a);
		copy_map(a);
		update_buffer(a);
		update_screen(a);
		SDL_BlitSurface(a->img, NULL, a->canvis, NULL);
		SDL_UpdateWindowSurface(a->win);
	}

	SDL_FreeSurface(a->img);
	SDL_FreeSurface(a->canvis);
	SDL_DestroyWindow(a->win);
	SDL_Quit();
}


float getPt(float n1, float n2, float perc)
{
	float diff = n2 - n1;

	return (float)n1 + (diff * perc);
}

void drawB(t_main *s, t_vector a, t_vector b, t_vector c) {
	for (float i = 0; i < 1; i += 0.001)
	{
		float xa = getPt(a.x, b.x, i);
		float ya = getPt(a.y, b.y, i);
		float xb = getPt(b.x, c.x, i);
		float yb = getPt(b.y, c.y, i);

		int x = getPt(xa, xb, i);
		int y = getPt(ya, yb, i);

		put_pixel(s, (int)x, (int)y, mkcol(255, 0, 0));
	}
}



int					WinMain(int ac, char **av)
{
	SDL_Init(SDL_INIT_EVERYTHING);
	a.win = SDL_CreateWindow("MOD 1 - Project", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, SCREENX, SCREENY, SDL_WINDOW_SHOWN);
	a.canvis = SDL_GetWindowSurface(a.win);

	startup(&a);
	draw_terrain(&a);

	//drawB(&a, mkvec(0, 350, 0), mkvec(250, 0, 0), mkvec(500, 50, 0));

	winctrl(&a);
}
