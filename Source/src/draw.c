#include "../includes/mod.h"

void		put_pixel(t_main *a, int x, int y, int n)
{
	if (x < SCREENX && y < SCREENY)
		if (x >= 0 && y >= 0)
			a->s_buffer[y][x] = n;
}

void		move_water_into_2darray(t_main *a, int x, int y, int n)
{
	if (n > a->f.w2d[y][x])
		a->f.w2d[y][x] = n;
}

void				draw_terrain(t_main *a)
{
	t_vector		p[16];
	static int		i;
	int				c;
	if (i++)
		return ;

	for (int z = 0; z < WORLDZ; z++) {
		for (int x = 0; x < WORLDX; x++) {
			for (int y = 0; y < WORLDY; y++) {
				if (y < a->f.h[z][x] || a->f.h[z][x] == 0) {
					if (a->f.t[z][y][x] > 0) {
						a->f.h[z][x] = y;
					}
				}
			}
		}
	}
	for (int z = 0; z < WORLDZ; z++) {
		if (z > 3) {
			for (int x = 0; x < WORLDX; x++) {
				if (x > 3 && x % 3 == 0 && z % 3 == 0)
				{
					mkpoint(a->f.h, x, z, p, a);
					surface(a, p);
				}
			}
		}
	}
	ft_memcpy(a->s_terrain, a->s_buffer, sizeof(int) * SCREENY * SCREENX);
}

void				draw_water(t_main *a, t_vector p1, t_vector t, int c)
{
	put_pixel(a, p1.x, p1.y, c);
	drawTriangles(a, t.x, t.y, t.z);
}

void		drawInvisibleTerrain(t_main *a, int x, int y, int z) {
	int		*rC;
	int		*rC2;
	int		x1;
	int		y1;
	int		x2;
	int		y2;

	rC = rotateScaledPoint(x, z, y);
	x1 = rC[0];
	y1 = rC[1];
	if (a->f.t[z][y][x] > 0) {
		if (a->f.t[z][y][x + 1] > 0) {
			rC2 = rotateScaledPoint(x + 1, z, y);
			x2 = rC2[0];
			y2 = rC2[1];
			draw_line(a, mkvec(x1, y1, 1), mkvec(x2, y2, 1), 0);
		}
		if (a->f.t[z + 1][y][x] > 0) {
			rC2 = rotateScaledPoint(x, z + 1, y);
			x2 = rC2[0];
			y2 = rC2[1];
			draw_line(a, mkvec(x1, y1, 1), mkvec(x2, y2, 1), 0);
		}
	}
}

void		copyTerrain(t_main *a) {
	for (int y = 0; y < SCREENY; y++) {
		for (int x = 0; x < SCREENX; x++) {
			if (a->s_buffer[y][x] == 0)
			a->s_buffer[y][x] = a->s_terrain[y][x];
		}
	}
}

void		update_buffer(t_main *a) {
	int		*rotatedCord;
	int		scaledX;
	int		scaledY;

	for (int y = 0; y < SCREENY; y++) {
		for (int x = 0; x < SCREENX; x++) {
			a->s_buffer[y][x] = 0;
		}
	}

	for (int z = 0; z < WORLDZ; z++) {
		for (int y = 0; y < WORLDY; y++) {
			for (int x = 0; x < WORLDX; x++) {
				rotatedCord = rotateScaledPoint(x, z, y);
				scaledX = rotatedCord[0];
				scaledY = rotatedCord[1];

				if (!((scaledX > SCREENX || scaledX < 0) || (scaledY > SCREENY || scaledY < 0))) {
					drawInvisibleTerrain(a, x, y, z);
					if (a->f.v[z][y][x] > 0) {
						draw_water(a, mkvec(scaledX, scaledY, 0), mkvec(x, y, z), 255);
					}
				} 
			}
		}
	}
	copyTerrain(a);
}

void				update_screen(t_main *a)
{
	unsigned int	*pixels;

	if (!a->img)
		a->img = SDL_CreateRGBSurface(0, SCREENX, SCREENY, 32, 0, 0, 0, 1);
	pixels = (unsigned int *)a->img->pixels;
	for (int y = 0; y < SCREENY; y++) {
		for (int x = 0; x < SCREENX; x++) {
			pixels[x + (y * SCREENX)] = a->s_buffer[y][x];
		}
	}
}
