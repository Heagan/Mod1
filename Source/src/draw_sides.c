#include "../includes/mod.h"

int					distance(t_vector v1, t_vector v2, t_vector v3)
{
	return (
	ft_abs(sqrt(sqr(v2.y - v1.y) + sqr(v2.x - v1.x))) < 25 &&
	ft_abs(sqrt(sqr(v2.y - v3.y) + sqr(v2.x - v3.x))) < 25 &&
	ft_abs(sqrt(sqr(v3.y - v1.y) + sqr(v3.x - v1.x))) < 25);
}

t_vector			check_side_x_n(t_main *a, t_vector t)
{
	t_vector		p2;

	if (a->f.v[t.z][t.y][t.x - 1] > 0)
	{
		int				*rotatedPoint;

		rotatedPoint = rotateScaledPoint(t.x - 1, t.z, t.y);
		p2.x = rotatedPoint[0];
		p2.y = rotatedPoint[1];
		return p2;
	}
	return t;
}

t_vector			check_side_x_p(t_main *a, t_vector t)
{
	t_vector		p2;

	if (a->f.v[t.z][t.y][t.x + 1] > 0)
	{
		int				*rotatedPoint;

		rotatedPoint = rotateScaledPoint(t.x + 1, t.z, t.y);
		p2.x = rotatedPoint[0];
		p2.y = rotatedPoint[1];
		return p2;
	}
	return t;
}

t_vector			check_side_z_n(t_main *a, t_vector t)
{
	t_vector		p2;

	if (a->f.v[t.z - 1][t.y][t.x] > 0)
	{
		int				*rotatedPoint;

		rotatedPoint = rotateScaledPoint(t.x, t.z - 1, t.y);
		p2.x = rotatedPoint[0];
		p2.y = rotatedPoint[1];
		return p2;
	}
	return t;
}

t_vector			check_side_z_p(t_main *a, t_vector t)
{
	t_vector		p2;

	if (a->f.v[t.z + 1][t.y][t.x] > 0)
	{
		int				*rotatedPoint;

		rotatedPoint = rotateScaledPoint(t.x, t.z + 1, t.y);
		p2.x = rotatedPoint[0];
		p2.y = rotatedPoint[1];
		return p2;
	}
	return t;
}

t_vector			check_side_y_n(t_main *a, t_vector t)
{
	t_vector		p2;

	if (a->f.v[t.z][t.y - 1][t.x] > 0)
	{
		int				*rotatedPoint;

		rotatedPoint = rotateScaledPoint(t.x, t.z, t.y - 1);
		p2.x = rotatedPoint[0];
		p2.y = rotatedPoint[1];
		return p2;
	}
	return t;
}

t_vector			check_side_y_p(t_main *a, t_vector t)
{
	t_vector		p2;

	if (a->f.v[t.z][t.y + 1][t.x] > 0)
	{
		int				*rotatedPoint;

		rotatedPoint = rotateScaledPoint(t.x, t.z, t.y + 1);
		p2.x = rotatedPoint[0];
		p2.y = rotatedPoint[1];
		return p2;
	}
	return t;
}

int				pointsValid(t_main *a, t_vector p1, t_vector p2, t_vector p3) {
	// printf("1 %i\n", a->s_buffer[p1.y][p1.x]);
	if (a->s_buffer[p1.y][p1.x] != 255) {
		return (0);
	}
	// printf("2 %i\n", a->s_buffer[p2.y][p2.x]);
	if (a->s_buffer[p2.y][p2.x] != 255) {
		return (0);
	}
	// printf("3 %i\n\n", a->s_buffer[p3.y][p3.x]);
	if (a->s_buffer[p3.y][p3.x] != 255) {
		return (0);
	}
	return (1);
}

void			drawYTriangles(t_main *a, int x, int y, int z, t_vector p2) {
	t_vector	p1 = mkvec(x, y, z);
	t_vector	p3 = mkvec(x, y, z);
	int			*rotatedPoint = NULL;
	int			colour = mkcol(0, 255 - (255 * (float)y / WORLDY), 255 * (float)y / WORLDY) * ((float)y * 1.5) / WORLDY;

	colour = 255;

	p3 = check_side_y_p(a, p1);

	if (!isVecEqual(p1, p3)) {
		rotatedPoint = rotateScaledPoint(x, z, y);
		p1.x = rotatedPoint[0];
		p1.y = rotatedPoint[1];
		drawTriangle(a, p1, p3, p2, colour);
	}

	p1 = mkvec(x, y, z);
	p3 = check_side_y_n(a, p1);

	if (!isVecEqual(p1, p3)) {
		rotatedPoint = rotateScaledPoint(x, z, y);
		p1.x = rotatedPoint[0];
		p1.y = rotatedPoint[1];
		drawTriangle(a, p1, p3, p2, colour);
	}
}

void			drawTriangles(t_main *a, int x, int y, int z) {
	t_vector	p1 = mkvec(x, y, z);
	t_vector	p2 = mkvec(x, y, z);
	t_vector	p3 = mkvec(x, y, z);
	int			*rotatedPoint = NULL;
	int			colour = mkcol(0, 255 - (255 * (float)y / WORLDY), 255 * (float)y / WORLDY) * ((float)y * 1.5) / WORLDY;

	colour = 255;

	p2 = check_side_x_n(a, p1);
	p3 = check_side_z_n(a, p1);
	
	if (!isVecEqual(p1, p2) && !isVecEqual(p1, p3)) {
		rotatedPoint = rotateScaledPoint(x, z, y);
		p1.x = rotatedPoint[0];
		p1.y = rotatedPoint[1];
		if (pointsValid(a, p1, p2, p3))
			drawTriangle(a, p1, p2, p3, colour);
		//drawYTriangles(a, x, y, z, p2);
		//drawYTriangles(a, x, y, z, p3);
	}
	
	p1 = mkvec(x, y, z);
	p2 = check_side_x_p(a, p1);
	p3 = check_side_z_p(a, p1);
	
	if (!isVecEqual(p1, p2) && !isVecEqual(p1, p3)) {
		rotatedPoint = rotateScaledPoint(x, z, y);
		p1.x = rotatedPoint[0];
		p1.y = rotatedPoint[1];
		if (pointsValid(a, p1, p2, p3))
			drawTriangle(a, p1, p2, p3, colour);
		//drawYTriangles(a, x, y, z, p2);
		//drawYTriangles(a, x, y, z, p3);
	}
}

