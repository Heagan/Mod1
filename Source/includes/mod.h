#ifndef MOD_H
# define MOD_H

# include "../libft/libft.h"
# include <SDL.h>
# include <math.h>
# include <stdlib.h>

# define SCREENX 500
# define SCREENY 500
# define WORLDX 50
# define WORLDY 50
# define WORLDZ 50
# define DENSITY 1000
# define DENSITYs WORLDX * WORLDY * WORLDZ /3
# define MAPQUALITY 0.01

typedef struct	s_line
{
	int			dx;
	int			dy;
	int			steps;
	float		xinc;
	float		yinc;
	float		x;
	float		y;
	int			x1;
	int			y1;
	int			x2;
	int			y2;
	int			i;
}				t_line;

typedef struct	s_point
{
	int			p1;
	int			p2;
	int			p3;
	int			p4;
}				t_point;

typedef struct	s_vector
{
	int			x;
	int			y;
	int			z;
	float		fx;
	float		fy;
}				t_vector;

typedef struct	s_fluid_cude
{
	int			size;
	int			dt;
	int			diff;
	int			visc;

	int			*s;
	int			*density;

	int			v[WORLDZ][WORLDY][WORLDX];
	int			v2[WORLDZ][WORLDY][WORLDX];
	int			t[WORLDZ][WORLDY][WORLDX];
	int			w2d[WORLDY][WORLDX];
	int			t2d[WORLDY][WORLDX];
	int			h[WORLDZ][WORLDX];

}				t_fluid_cube;

typedef struct		s_main
{
	SDL_Window		*win;
	SDL_Renderer	*renderer;
	SDL_Event		event;
	SDL_Surface		*img;
	SDL_Surface		*canvis;
	t_fluid_cube	f;
	int				s_buffer[SCREENY][SCREENX];
	int				s_terrain[SCREENY][SCREENX];
}					t_main;

float			mkcol(float r, float g, float b);
void			refresh(t_main *a);
void			copy_map(t_main *a);
void			put_pixel(t_main *a, int x, int y, int n);
void			update_screen(t_main *a);
void			update_buffer(t_main *a);
int				check_diag_x(t_main *a, t_vector t);
int				check_diag_z(t_main *a, t_vector t);
int				check_side_x(t_main *a, t_vector t);
int				check_side_z(t_main *a, t_vector t);
int				check_below(t_main *a, t_vector t);
int				check_above(t_main *a, t_vector t);
int				*rotatePoint(int x, int y);
int				*rotateScaledPoint(int x, int y, int z);
void			build_terrain(t_main *a);
void			spawn_emitters(t_main *a);
void			reset(t_main *a);
void			surface(t_main *a, t_vector p[16]);
t_vector		mkvec(int x, int y, int z);
t_vector		calc_bezier_qua(t_vector p[4], float t);
t_vector		curve(t_main *a, t_vector p[16], float u, float v);
t_vector		check_side_x_n(t_main *a, t_vector t);
t_vector		check_side_x_p(t_main *a, t_vector t);
t_vector		check_side_z_n(t_main *a, t_vector t);
t_vector		check_side_z_p(t_main *a, t_vector t);
void			drawTriangles(t_main *a, int x, int y, int z);
t_point			set_point(t_point p, int t, int d);
void			draw_terrain(t_main *a);
void			reset_p(t_point *p);
void			set_p(t_point *p, int n);
void			mkpoint(int h[WORLDZ][WORLDX], int xMax, int zMax, t_vector *p, t_main *a);
void			drawTriangle(t_main *a, t_vector v1, t_vector v2, t_vector v3, int colour);
void			draw_line(t_main *a, t_vector p1, t_vector p2, int colour);
void			print_2d_int_array(int array[WORLDY][WORLDX]);
float			mkcolAlpha(float a, float r, float g, float b);
int				isVecEqual(t_vector a, t_vector b);

#endif