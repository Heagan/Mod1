#include "mod.h"

int			distance(t_vector v1, t_vector v2, t_vector v3)
{
	return (
	ft_abs(sqrt(sqr(v2.y - v1.y) + sqr(v2.x - v1.x))) < 25 &&
	ft_abs(sqrt(sqr(v2.y - v3.y) + sqr(v2.x - v3.x))) < 25 &&
	ft_abs(sqrt(sqr(v3.y - v1.y) + sqr(v3.x - v1.x))) < 25);
}

int			get_side_x_n(t_main *a, t_vector p, t_vector t)
{
	return 1;
}

int			get_side_x_p(t_main *a, t_vector p, t_vector t)
{
return 1;
}

int			get_side_z_n(t_main *a, t_vector p, t_vector t)
{
return 1;
}

int			get_side_z_p(t_main *a, t_vector p, t_vector t)
{
return 1;
}
