#include "mod.h"

int				rotate_point(int x, int y, int s)
{
	if (s == 1)
		return ((x * 3) - (y * 3) + 350);
	else
		return (round(((x * 3) + (y * 3)) / 2));
}

t_point				set_point(t_point p, int t, int d)
{
	if (p.p1 == 0)
		p.p1 = d;
	else if (p.p2 == 0)
		p.p2 = d;
	else if (p.p3 == 0)
		p.p3 = d;
	else
		p.p4 = d;
	return (p);
}

void				reset_p(t_point *p)
{
	p->p1 = 0;
	p->p2 = 0;
	p->p3 = 0;
	p->p4 = 0;
}

void				set_p(t_point *p, int n)
{
	if (p->p1 == 0)
		p->p1 = n;
	else if (p->p2 == 0)
		p->p2 = n;
	else if (p->p3 == 0)
		p->p3 = n;
	else
		p->p4 = n;
}

void				mkpoint(int h[WORLDZ][WORLDX], int x, int z, t_vector *p)
{
	t_vector		t;
	int				i;

	i = 0;
	t.x = x;
	t.z = z - 5;
	while (++t.z < z)
	{
		t.x = x - 5;
		while (++t.x < x)
		{
			p[i].x = ft_clamp(0, SCREENX, rotate_point(t.x, t.z, 1));
			p[i].y = ft_clamp(0, SCREENY, rotate_point(t.x, t.z, 0) + h[t.z][t.x] * 5);
			p[i].z = h[t.z][t.x];
			i++;
		}
	}
}
