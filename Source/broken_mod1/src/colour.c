#include "mod.h"

float		mkcol(float r, float g, float b)
{
	r = ft_clamp(0, 255, r);
	g = ft_clamp(0, 255, g);
	b = ft_clamp(0, 255, b);
	return (r * 256 * 256 + g * 256 + b);
}