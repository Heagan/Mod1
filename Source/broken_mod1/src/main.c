#include "mod.h"

void				ft_error(char *s)
{
	ft_putendl(s);
	exit (-1);
}

void				startup(t_main *a)
{
	reset_3d_maps(a);
	reset_2d_maps(a);
	build_terrain(a);
	spawn_emitters(a);
}

void				winctrl(t_main *a)
{
	int				run;

	run = 1;
	while (run)
	{
		while (SDL_PollEvent(&a->event))
			if (a->event.type == SDL_QUIT)
				run = 0;
		refresh(a);
		copy_map(a);
		update_buffer(a);
		update_screen(a);
		SDL_BlitSurface(a->img, NULL, a->canvis, NULL);
		SDL_UpdateWindowSurface(a->win);
	}
	SDL_FreeSurface(a->img);
	SDL_FreeSurface(a->canvis);
	SDL_DestroyWindow(a->win);
	SDL_Quit();
}

int					main(int ac, char **av)
{
	static t_main	a;
	float			f;

	ft_putendl("INIT SDL");

	SDL_Init(SDL_INIT_EVERYTHING);
	a.win = SDL_CreateWindow("MOD 1 - Project", SDL_WINDOWPOS_UNDEFINED,
	SDL_WINDOWPOS_UNDEFINED, SCREENX, SCREENY,
	SDL_WINDOW_OPENGL);
	a.canvis = SDL_GetWindowSurface(a.win);

	ft_putendl("STARTING UP");
	/*
	* SET MAP VAR TO 0
	* SAVE TERRAIN INTO T ARRAY
	* SPAWN EMMITERS
	*/
	startup(&a);
	/*
	* 
	*/
	draw_terrain(&a);
	ft_putendl("START UP SUCCESS!!!");
	winctrl(&a);
}
