#include "mod.h"

void				appened_surface(t_vector pu[16], t_vector p[4], int n)
{
	int		i;

	i = -1;
	while (++i < 4)
		pu[n * 4 + i] = p[i];
}

void				draw_water(t_main *a, t_vector p1, t_vector t, int c)
{
	put_pixel(a, p1.x, p1.y, c);
}

void				update_buffer(t_main *a)
{
	t_vector		t;
	t_vector		d;

	t.y = -1;
	while (++t.y < SCREENY && (t.x = -1 != -2))
		while (++t.x < SCREENX)
			a->s_buffer[t.y][t.x] = 0;

	t.z = -1;
	while (++t.z < WORLDZ)
	{
		t.y = -1;
		while (++t.y < WORLDY)
		{
			t.x = -1;
			while (++t.x < WORLDX)
			{
				if (a->f.v[t.z][t.y][t.x] > 0)
				{
					d.x = rotate_point(t.x , t.z, 1);
					d.y = rotate_point(t.x , t.z, 0) + t.y * 5;
					/*if (d.x > 0 && d.x < SCREENX)
						if (d.y > 0 && d.y < SCREENY)*/
								draw_water(a, d, t, 0xFFFFFF);
				}

				if (a->f.v[t.z][t.y][t.x] > 0)
					put_pixel(a, t.x, t.y, 0x0000ff);
				if (a->f.t[t.z][t.y][t.x] > 0)
					put_pixel(a, t.x, t.y, 0x00FF00);
				if (a->f.v[t.y][t.z][t.x] > 0)
					put_pixel(a, t.x + WORLDX, t.y, 0x0000ff);
				if (a->f.t[t.y][t.z][t.x] > 0)
					if (a->s_buffer[t.y][t.x + WORLDX] != 255)
						put_pixel(a, t.x + WORLDX, t.y, 0x00FF00);
			}
		}
	}
}
