#include "mod.h"

void		put_pixel(t_main *a, int x, int y, int n)
{
	if (x <= SCREENX && y <= SCREENY)
		if (x >= 0 && y >= 0)
			a->s_buffer[y][x] = n;
}

void				update_screen(t_main *a)
{
	SDL_Surface		*surface;
	unsigned int	*pixels;
	int				x;
	int				y;

	surface = SDL_CreateRGBSurface(0, SCREENX, SCREENY, 32, 0, 0, 0, 1);
	pixels = (unsigned int *)surface->pixels;
	y = -1;
	while (++y < SCREENY && (x = -1 != -2))
		while (++x < SCREENX)
			pixels[x + (y * SCREENX)] = a->s_buffer[y][x];
	a->img = surface;
}