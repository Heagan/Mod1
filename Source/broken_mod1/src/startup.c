#include "mod.h"

void				build_terrain(t_main *a)
{
	int				x;
	int				y;
	int				z;
	int				n;

	z = -1;
	while (++z < WORLDZ)
	{
		x = -1;
		while (++x < WORLDX)
		{
			//y = ft_clamp(0, WORLDY, 8 * cos(x/4)) + (8 * cos(z/4));
			//y = (8 * sin(x/2));
			//y = ft_clamp(15, WORLDY - 1, y + WORLDY - 1);//ft_clamp(15, WORLDY, y + WORLDY - 5);
			y = WORLDY - 5;
			if (y >= 0 && y < WORLDY)
				a->f.t[z][y][x] = 1;
		}
	}

	z = -1;
	while (++z < WORLDZ)
	{
		x = -1;
		while (++x < WORLDX)
		{
			y = -1;
			while (++y < WORLDY)
				if (a->f.t[z][y][x] == 1 && (n = y) == y)
					while (++n < WORLDY)
						a->f.t[z][n][x] = 1;
		}
	}
}

void				spawn_emitters(t_main *a)
{
	t_vector		t;

	// EMMITTERS!!
	a->f.v[0][0][0] = -1;
	a->f.v[0][0][WORLDX - 1] = -1;
	a->f.v[WORLDX - 1][0][0] = -1;
	a->f.v[0][0][WORLDX / 2] = -1;
	a->f.v[WORLDX / 2][0][0] = -1;

	t.z = -1;
	while (++t.z < WORLDZ)
	{
		t.y = -1;
		while (++t.y < WORLDY)
		{
			t.x = -1;
			while (++t.x < WORLDX)
			{
				if (a->f.v[t.z][t.y][t.x] == -1)
					a->f.v[t.z][t.y][t.x] = DENSITY;
			}
		}
	}
}

void				reset_2d_maps(t_main *a)
{
	int				x;
	int				y;

	y = -1;
	while (++y < SCREENY)
	{
		x = -1;
		while (++x < SCREENX)
		{
			a->f.t2d[y][x] = 0;
		}
	}
}

void				reset_3d_maps(t_main *a)
{
	int				x;
	int				y;
	int				z;

	z = -1;
	while (++z < WORLDZ)
	{
		y = -1;
		while (++y < WORLDY)
		{
			x = -1;
			while (++x < WORLDX)
			{
				a->f.v[z][y][x] = 0;
				a->f.v2[z][y][x] = 0;
				a->f.t[z][y][x] = 0;
			}
		}
	}
}
