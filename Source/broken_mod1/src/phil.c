#include "mod.h"


void	refresh(t_main *a)
{
	int	x;
	int	y;
	int	z;

	z = -1;
	while (++z < WORLDZ)
	{
		y = -1;
		while (++y < WORLDY)
		{
			x = -1;
			while (++x < WORLDX)
			{
				if (a->f.v[z][y][x])
				{
					if (!check_below(a, mkvec(x, y, z)))
						if (!check_side_x(a, mkvec(x, y, z)))
					//		if (!check_side_z(a, mkvec(x, y, z)))
					//			if (!check_diag_x(a, mkvec(x, y, z)))
					//				if (!check_diag_z(a, mkvec(x, y, z)))
										check_above(a, mkvec(x, y, z));
					if (a->f.v[z][y][x])
					{
						a->f.v2[z][y][x] = a->f.v[z][y][x];
						a->f.v[z][y][x] = 0;
					}
					x++;
				}
			}
		}
	}
}

void	copy_map(t_main *a)
{
	int	x;
	int	y;
	int	z;

	z = -1;
	while (++z < WORLDZ)
	{
		y = -1;
		while (++y < WORLDY)
		{
			x = -1;
			while (++x < WORLDX)
			{
				//if (a->f.v2[z][y][x] > 0)
				{
					a->f.v[z][y][x] = a->f.v2[z][y][x];
					a->f.v2[z][y][x] = 0;
				}
			}
		}
	}
}
