#include "mod.h"

t_vector		curve(t_main *a, t_vector p[16], float u, float v)
{
	t_vector	cu[4];
	t_vector	pu[4];
	int			i;
	
	i = -1;
	while (++i < 4)
	{
		cu[0] = p[i * 4];
		cu[1] = p[i * 4 + 1];
		cu[2] = p[i * 4 + 2];
		cu[3] = p[i * 4 + 3];
		pu[i] = calc_bezier_qua(cu, u);
	}
	return (calc_bezier_qua(pu, v));
}

t_vector		calc_bezier_cue(t_vector p[3], float t)
{
	t_vector	d;

	d.x = (1 - t) * (1 - t) * p[0].x + 2 * (1 - t) * (t) * p[1].x + (t * t * p[2].x);
	d.y = (1 - t) * (1 - t) * p[0].y + 2 * (1 - t) * (t) * p[1].y + (t * t * p[2].y);
	return (d);
}

t_vector		calc_bezier_qua(t_vector p[4], float t)
{
	t_vector	d;

	d.x = (1 - t) * (1 - t) * (1 - t) * p[0].x +
	3 * (1 - t) * (1 - t) * t * p[1].x +
	3 * (1 - t) * t * t * p[2].x +
	t * t * t * p[3].x;
	
	d.y = (1 - t) * (1 - t) * (1 - t) * p[0].y +
	3 * (1 - t) * (1 - t) * t * p[1].y +
	3 * (1 - t) * t * t * p[2].y +
	t * t * t * p[3].y;

	return (d);
}

void			water_surface(t_main *a, t_vector p[16])
{
	float		u;
	float		v;
	t_vector	pu;

	v = 0;
	while (v <= 1)
	{
		v += 0.01;
		u = 0;
		while (u <= 1)
		{
			u += 0.01;
			pu = curve(a, p, u, v);
			put_pixel(a, pu.x, pu.y, mkcol(0, 0, 255 * ((float)p[(int)((float)(16 * (u * v)))].z / WORLDY)));
		}
	}
}

void			surface(t_main *a, t_vector p[16])
{
	float		u;
	float		v;
	t_vector	pu;

	v = 0;
	while (v <= 1)
	{
		v += 0.01;
		u = 0;
		while (u <= 1)
		{
			u += 0.01;
			pu = curve(a, p, u, v);
			a->f.t2d[pu.y][pu.x] = p[(int)((float)(16 * (u * v)))].z;
			put_pixel(a, pu.x, pu.y, 0x00FFFF);
		}
	}
}
