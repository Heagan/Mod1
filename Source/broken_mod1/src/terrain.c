#include "mod.h"

void				draw_terrain(t_main *a)
{
	t_vector		t;
	t_vector		p[16];
	int				h[WORLDZ][WORLDX];
	int				c;

	//Clear Terrain Data
	t.z = -1;
	while (++t.z < WORLDZ)
	{
		t.x = -1;
		while (++t.x < WORLDX)
			h[t.z][t.x] = WORLDY;
	}

	//FILL H ARRAY FROM T ARRAY
	t.z = -1;
	while (++t.z < WORLDZ)
	{
		t.x = -1;
		while (++t.x < WORLDX)
		{
			t.y = -1;
			while (++t.y < WORLDY)
				if (t.y < h[t.z][t.x] || h[t.z][t.x] == 0)
					if (a->f.t[t.z][t.y][t.x] > 0)
						h[t.z][t.x] = t.y;
		}
	}

	// MAKING TERRAIN AND STORING POINTS INTO T2D ARRAY
	t.z = -1;
	while (++t.z < WORLDZ)
	{
		if (t.z > 3)
		{
			t.x = -1;
			while (++t.x < WORLDX)
			{
				if (t.x > 3 && t.x % 3 == 0 && t.z % 3 == 0)
				{
					mkpoint(h, t.x, t.z, p);
					surface(a, p);
				}
			}
		}
	}

	ft_memcpy(a->s_terrain , a->s_buffer, SCREENX * SCREENY);

}


int					is_terrain_clear(t_main *a, t_vector t)
{
	t_vector		v;
	t_vector		d;
	int				y;
	int				p_water;
	int				p_land;

	v = t;
	p_water = t.x + t.z + (WORLDY - t.y);
	while ((++v.x < WORLDX && ++v.z < WORLDZ) && (++v.x < t.x + 9 && ++v.z < t.z + 9))
	{
		y = WORLDY;
		while (--y >= 0)
			if (a->f.t[v.z][y][v.x] != 0)
				v.y = y;
		p_land = v.x + v.z + (WORLDY - v.y);
		if (p_land > p_water && v.y + 50 < t.y)
		{
			d.x = rotate_point(v.x, v.z, 1);
			d.y = rotate_point(v.x, v.z, 0) + (v.y) * 5;
			put_pixel(a, d.x, d.y, 0xFF0000);
			return (0);
		}
	}
	return (1);
}