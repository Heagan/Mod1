#include <OpenGL/gl.h>

static void	init_gl(void)
{
	float	light_col[4];

	light_col[0] = 1.0;
	light_col[1] = 1.0;
	light_col[2] = 1.0;
	light_col[3] = 1.0;
	glClearColor(0.0, 0.0, 0.0, 1.0);
	glMatrixMode(GL_PROJECTION);
	gluPerspective(45,640.0/480.0,1.0,500.0);
	glMatrixMode(GL_MODELVIEW);
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_LIGHTING);
	glEnable(GL_LIGHT0);
	// glLightfv(GL_LIGHT0, GL_DIFFUSE, light_col);
	glDepthFunc(GL_LESS);
	glClear(GL_COLOR_BUFFER_BIT);
}

int draw(GLuint texture, t_obj_data *obj_data)
{
	int	num;
	num = glGenLists(1);
	glNewList(num, GL_COMPILE);
		glBegin(GL_QUADS);
			glVertex3f(-25.0f, 0.0f, 25.0f);
			glNormal3f(0.0f, -1.0f, -0.0f);
			glTexCoord3f(1.0f, 0.0f, 0.0f);
			glVertex3f(-25.0f, 0.0f, -25.0f);
			glNormal3f(0.0f, -1.0f, -0.0f);
			glTexCoord3f(1.0f, 1.0f, 0.0f);
			glVertex3f(25.0f, 0.0f, -25.0f);
			glNormal3f(0.0f, -1.0f, -0.0f);
			glTexCoord3f(0.0f, 1.0f, 0.0f);
			glVertex3f(25.0f, 0.0f, 25.0f);
			glNormal3f(0.0f, -1.0f, -0.0f);
			glTexCoord3f(0.0f, 0.0f, 0.0f);

			glVertex3f(-25.0f, 50.0f, 25.0f);
			glNormal3f(0.0f, 1.0f, -0.0f);
			glTexCoord3f(0.0f, 0.0f, 0.0f);
			glVertex3f(25.0f, 50.0f, 25.0f);
			glNormal3f(0.0f, 1.0f, -0.0f);
			glTexCoord3f(1.0f, 0.0f, 0.0f);
			glVertex3f(25.0f, 50.0f, -25.0f);
			glNormal3f(0.0f, 1.0f, -0.0f);
			glTexCoord3f(1.0f, 1.0f, 0.0f);
			glVertex3f(-25.0f, 50.0f, -25.0f);
			glNormal3f(0.0f, 1.0f, -0.0f);
			glTexCoord3f(0.0f, 1.0f, 0.0f);

			glVertex3f(-25.0f, 0.0f, 25.0f);
			glNormal3f(0.0f, 0.0f, 1.0f);
			glTexCoord3f(0.0f, 0.0f, 0.0f);
			glVertex3f(25.0f, 0.0f, 25.0f);
			glNormal3f(0.0f, 0.0f, 1.0f);
			glTexCoord3f(1.0f, 0.0f, 0.0f);
			glVertex3f(25.0f, 50.0f, 25.0f);
			glNormal3f(0.0f, 0.0f, 1.0f);
			glTexCoord3f(1.0f, 1.0f, 0.0f);
			glVertex3f(-25.0f, 50.0f, 25.0f);
			glNormal3f(0.0f, 0.0f, 1.0f);
			glTexCoord3f(0.0f, 1.0f, 0.0f);

			glVertex3f(25.0f, 0.0f, 25.0f);
			glNormal3f(1.0f, 0.0f, -0.0f);
			glTexCoord3f(0.0f, 0.0f, 0.0f);
			glVertex3f(25.0f, 0.0f, -25.0f);
			glNormal3f(1.0f, 0.0f, -0.0f);
			glTexCoord3f(1.0f, 0.0f, 0.0f);
			glVertex3f(25.0f, 50.0f, -25.0f);
			glNormal3f(1.0f, 0.0f, -0.0f);
			glTexCoord3f(1.0f, 1.0f, 0.0f);
			glVertex3f(25.0f, 50.0f, 25.0f);
			glNormal3f(1.0f, 0.0f, -0.0f);
			glTexCoord3f(0.0f, 1.0f, 0.0f);

			glVertex3f(25.0f, 0.0f, -25.0f);
			glNormal3f(0.0f, 0.0f, -1.0f);
			glTexCoord3f(0.0f, 0.0f, 0.0f);
			glVertex3f(-25.0f, 0.0f, -25.0f);
			glNormal3f(0.0f, 0.0f, -1.0f);
			glTexCoord3f(1.0f, 0.0f, 0.0f);
			glVertex3f(-25.0f, 50.0f, -25.0f);
			glNormal3f(0.0f, 0.0f, -1.0f);
			glTexCoord3f(1.0f, 1.0f, 0.0f);
			glVertex3f(25.0f, 50.0f, -25.0f);
			glNormal3f(0.0f, 0.0f, -1.0f);
			glTexCoord3f(0.0f, 1.0f, 0.0f);

			glVertex3f(-25.0f, 0.0f, -25.0f);
			glNormal3f(-1.0f, 0.0f, -0.0f);
			glTexCoord3f(0.0f, 0.0f, 0.0f);
			glVertex3f(-25.0f, 0.0f, 25.0f);
			glNormal3f(-1.0f, 0.0f, -0.0f);
			glTexCoord3f(1.0f, 0.0f, 0.0f);
			glVertex3f(-25.0f, 50.0f, 25.0f);
			glNormal3f(-1.0f, 0.0f, -0.0f);
			glTexCoord3f(1.0f, 1.0f, 0.0f);
			glVertex3f(-25.0f, 50.0f, -25.0f);
			glNormal3f(-1.0f, 0.0f, -0.0f);
			glTexCoord3f(0.0f, 1.0f, 0.0f);
		glEnd();
	glEndList();
	return(num);
}

void	display(int *gl_list, int rot_angle)
{
	float pos[4];

	pos[0] = -1.0;
	pos[1] = 1.0;
	pos[2] = -2.0;
	pos[3] = 1.0;
	glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT);
	glLoadIdentity();
	glLightfv(GL_LIGHT0, GL_POSITION, pos);
	glTranslatef(0.0, -50.0, -150.0);
	glRotatef(rot_angle, 0.0, 1.0, 0.0);
	glCallList(*gl_list);
}

int	main(void)
{
	t_scr		scr;
	t_obj_data	obj_data;

	init(&scr, &obj_data);
	if (read_obj_file(&obj_data) == -1)
		return (-1);
	while (!scr.quit)
	{
		while (SDL_PollEvent(&scr.event))
		{
			if (scr.event.type == SDL_QUIT)
				scr.quit = 1;
			else if (scr.event.type == SDL_KEYDOWN)
			{
				if (scr.event.key.keysym.sym == SDLK_ESCAPE)
					scr.quit = 1;
			}
		}
		scr.gl_list = draw(scr.texture, &obj_data);
		display(&scr.gl_list, scr.rot_angle);
		SDL_GL_SwapWindow(scr.win);
		scr.rot_angle += 1.0;
		if (scr.rot_angle > 360)
			scr.rot_angle -= 360;
	}
	return (0);
}
