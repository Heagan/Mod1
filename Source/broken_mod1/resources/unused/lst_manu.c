#include "mod.h"

int			get_elem(t_main *a, int x, int y)
{
	t_drop	*h;

	h = a->f.v;
	while (h)
	{
		if (h->x == x && h->y == y)
			return (h->value);
		h = h->next;
	}
	return (-1);
}

int			append_elem(t_main *a, int x, int y, int value)
{
	t_drop	*h;

	h = a->f.v;
	while (h)
	{
		if (h->x == x && h->y == y)
		{
			h->value += value;
			h->moved = ft_tog(h->moved);
			return (1);
		}
		h = h->next;
	}
	ft_error("Trying to alter a number Out of range!");
	return (-1);
}

int			set_elem(t_main *a, int x, int y, int value)
{
	t_drop	*h;

	h = a->f.v;
	while (h)
	{
		if (h->x == x && h->y == y)
		{
			h->value = value;
			h->moved = ft_tog(h->moved);
			return (1);
		}
		h = h->next;
	}
	ft_error("Trying to alter a number Out of range!");
	return (-1);
}

void		appened_list(t_drop **r, t_drop *d)
{
	t_drop	*tmp;

	tmp = *r;
	if (!*r)
	{
		*r = d;
		return ;
	}
	while (tmp->next)
		tmp = tmp->next;
	tmp->next = d;
	d->prev = tmp;
}

void		init_elem(t_drop *d, int x, int y)
{

	d->x = x;
	d->y = y;
	d->value = 0;
	d->moved = 0;
	d->next = NULL;
}

void		add_node(t_drop **head, int x, int y)
{
	t_drop	*d;

	d = (t_drop *)malloc(sizeof(t_drop));
	init_elem(d, x, y);
	appened_list(head, d);
}