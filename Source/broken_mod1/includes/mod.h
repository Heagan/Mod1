#ifndef MOD_H
# define MOD_H

# include "../libft/libft.h"
# include <SDL2/SDL.h>
# include <math.h>
# include <stdlib.h>

# define SCREENX 800
# define SCREENY 640
# define WORLDX 80
# define WORLDY 80
# define WORLDZ 80
# define DENSITY 100
# define DENSITYs WORLDX * WORLDY * WORLDZ /3
# define DEG_T_RAD(x) (M_PI / 180) * x
# define COL_ANGLE tan(DEG_T_RAD(5))

typedef struct	s_line
{
	int			dx;
	int			dy;
	int			steps;
	float		xinc;
	float		yinc;
	float		x;
	float		y;
	int			x1;
	int			y1;
	int			x2;
	int			y2;
	int			i;
}				t_line;

typedef struct	s_point
{
	int			p1;
	int			p2;
	int			p3;
	int			p4;
}				t_point;

typedef struct	s_vector
{
	int			x;
	int			y;
	int			z;
	float		fx;
	float		fy;
}				t_vector;

typedef struct	s_fluid_cude
{
	int			size;
	int			dt;
	int			diff;
	int			visc;

	int			*s;
	int			*density;

	int			v[WORLDZ][WORLDY][WORLDX];
	int			v2[WORLDZ][WORLDY][WORLDX];
	int			t[WORLDZ][WORLDY][WORLDX];
	int			t2d[SCREENY][SCREENX];

}				t_fluid_cube;

typedef struct		s_main
{
	SDL_Window		*win;
	SDL_Renderer	*renderer;
	SDL_Event		event;
	SDL_Surface		*img;
	SDL_Surface		*canvis;
	t_fluid_cube	f;
	int				s_buffer[SCREENY][SCREENX];
	int				s_terrain[SCREENY][SCREENX];
}					t_main;

float			mkcol(float r, float g, float b);
void			refresh(t_main *a);
void			copy_map(t_main *a);
void			put_pixel(t_main *a, int x, int y, int n);
void			update_screen(t_main *a);
void			update_buffer(t_main *a);
int				check_diag_x(t_main *a, t_vector t);
int				check_diag_z(t_main *a, t_vector t);
int				check_side_x(t_main *a, t_vector t);
int				check_side_z(t_main *a, t_vector t);
int				side_clear(t_main *a, t_vector t, int side);
int				check_below(t_main *a, t_vector t);
int				check_above(t_main *a, t_vector t);
void			build_terrain(t_main *a);
void			spawn_emitters(t_main *a);
void			reset_3d_maps(t_main *a);
void			reset_2d_maps(t_main *a);
void			surface(t_main *a, t_vector p[16]);
t_vector		mkvec(int x, int y, int z);
t_vector		calc_bezier_qua(t_vector p[4], float t);
t_vector		curve(t_main *a, t_vector p[16], float u, float v);
t_vector		calc_bezier_cue(t_vector p[3], float t);
t_point			set_point(t_point p, int t, int d);
int				rotate_point(int x, int y, int s);
void			draw_terrain(t_main *a);
void			reset_p(t_point *p);
void			set_p(t_point *p, int n);
void			mkpoint(int h[WORLDZ][WORLDX], int x, int z, t_vector *v);
void			move_water(t_main *a);
int				get_side_x_n(t_main *a, t_vector p, t_vector t);
int				get_side_x_p(t_main *a, t_vector p, t_vector t);
int				get_side_z_n(t_main *a, t_vector p, t_vector t);
int				get_side_z_p(t_main *a, t_vector p, t_vector t);
void			draw_triangle(t_main *a, t_vector v1, t_vector v2, t_vector v3);
void			draw_line(t_main *a, t_vector p1, t_vector p2);
int				is_terrain_clear(t_main *a, t_vector t);
int				v_cross(t_vector v1, t_vector v2);
void			water_surface(t_main *a, t_vector p[16]);

#endif