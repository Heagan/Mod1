#include "mod.h"

void				refresh(t_main *a)
{
	SDL_Surface		*surface;
	unsigned int	*pixels;
	int				x;
	int				y;

	surface = SDL_CreateRGBSurface(0, SCREENX, SCREENY, 32, 0, 0, 0, 1);
	pixels = (unsigned int *)surface->pixels;
	y = -1;
	while (++y < SCREENY && (x = -1 != -2))
		while (++x < SCREENX)
			pixels[x + (y * SCREENX)] = a->s_buffer[y][x];
	a->img = surface;
}