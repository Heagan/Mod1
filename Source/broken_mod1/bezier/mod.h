#ifndef MOD_H
# define MOD_H

# include "../libft/libft.h"
# include <SDL2/SDL.h>
# include <math.h>
# include <stdlib.h>

# define SCREENX 800
# define SCREENY 640
# define WORLDX 25
# define WORLDY 25
# define WORLDZ 25
# define DENSITY 50000

typedef struct	s_vector
{
	int			x;
	int			y;
	int			z;
}				t_vector;

typedef struct	s_fluid_cude
{
	int			size;
	int			dt;
	int			diff;
	int			visc;

	int			*s;
	int			*density;

	int			v[WORLDZ][WORLDY][WORLDX];
	int			v2[WORLDZ][WORLDY][WORLDX];
	int			t[WORLDZ][WORLDY][WORLDX];

}				t_fluid_cube;

typedef struct		s_main
{
	SDL_Window		*win;
	SDL_Renderer	*renderer;
	SDL_Event		event;
	SDL_Surface		*img;
	SDL_Surface		*canvis;
	int				s_buffer[SCREENY][SCREENX];
	t_fluid_cube	f;
}					t_main;

void		triangle(t_main *a, t_vector v1, t_vector v2, t_vector v3);
void		refresh(t_main *a);
void		put_pixel(t_main *a, int x, int y, int n);
void		square(t_main *a, t_vector v1, t_vector v2);

#endif