/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_next_line.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gsferopo <gsferopo@42.fr>                  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/07/22 12:56:24 by bmoodley          #+#    #+#             */
/*   Updated: 2017/12/07 14:32:48 by gsferopo         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

static void	build(char **line, char **neww, char *pos)
{
	*line = ft_strsub(*neww, 0, ft_strlen(*neww) - ft_strlen(pos));
	*neww = ft_strdup(pos + 1);
}

static int	ft_append(int fd, char **neww)
{
	char		*buf;
	char		*temp;
	int			ret;

	buf = (char *)malloc(sizeof(char) * (BUFF_SIZE + 1));
	if (buf == NULL)
		return (-1);
	ret = read(fd, buf, BUFF_SIZE);
	if (ret > 0)
	{
		buf[ret] = '\0';
		temp = ft_strjoin(*neww, buf);
		free(*neww);
		*neww = temp;
	}
	free(buf);
	return (ret);
}

int			get_next_line(const int fd, char **line)
{
	static char *neww = NULL;
	char		*pos;
	int			ret;

	if (neww == NULL)
		neww = ft_strnew(0);
	pos = ft_strchr(neww, '\n');
	while (pos == NULL)
	{
		ret = ft_append(fd, &neww);
		if (ret == 0)
		{
			if (ft_strlen(neww) == 0)
				return (0);
			neww = ft_strjoin(neww, "\n");
		}
		if (ret < 0)
			return (-1);
		else
			pos = ft_strchr(neww, '\n');
	}
	build(line, &neww, pos);
	return (1);
}
