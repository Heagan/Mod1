#include "mod.h"

typedef struct	s_point
{
	int			x;
	int			y;
}				point;

void		put_pixel(t_main *a, int x, int y, int n)
{
	a->s_buffer[y][x] = n;
}

float		calc_bezier(t_main *a, float p1, float p2, float p3, float t)
{
	return ((1 - t) * (1 - t) * p1 + 2 * (1 - t) * (t) * p2 + (t * t * p3));
}

point		calc_bezier_qua(point p[4], float t)
{
	point	d;

	d.x = (1 - t) * (1 - t) * (1 - t) * p[0].x +
	3 * (1 - t) * (1 - t) * t * p[1].x +
	3 * (1 - t) * t * t * p[2].x +
	t * t * t * p[3].x;
	
	d.y = (1 - t) * (1 - t) * (1 - t) * p[0].y +
	3 * (1 - t) * (1 - t) * t * p[1].y +
	3 * (1 - t) * t * t * p[2].y +
	t * t * t * p[3].y;
	
	return (d);
}

point				curve(t_main *a, point p[16], float u, float v)
{
	point			cu[4];
	point			pu[4];
	int				i;
	
	i = -1;
	while (++i < 4)
	{
		cu[0] = p[i * 4];
		cu[1] = p[i * 4 + 1];
		cu[2] = p[i * 4 + 2];
		cu[3] = p[i * 4 + 3];
		pu[i] = calc_bezier_qua(cu, u);
	}
	return (calc_bezier_qua(pu, v));
}

void				surface(t_main *a, point p[16])
{
	float	u;
	float	v;
	point	pu;

	v = 0;
	while (v <= 1)
	{
		v += 0.001;
		u = 0;
		while (u <= 1)
		{
			u += 0.001;
			pu = curve(a, p, u, v);
			put_pixel(a, pu.x, pu.y, 255);
		}
	}
}

void				mkpoint(point	*p)
{
	p[0].x = 50;
	p[1].x = 150;
	p[2].x = 250;
	p[3].x = 350;

	p[4].x = 50;
	p[5].x = 150;
	p[6].x = 250;
	p[7].x = 350;

	p[8].x = 50;
	p[9].x = 150;
	p[10].x = 250;
	p[11].x = 350;

	p[12].x = 50;
	p[13].x = 150;
	p[14].x = 250;
	p[15].x = 350;

	p[0].y = 50;
	p[1].y = 150;
	p[2].y = 150;
	p[3].y = 50;

	p[4].y = 150;
	p[5].y = 250;
	p[6].y = 250;
	p[7].y = 150;

	p[8].y = 150;
	p[9].y = 250;
	p[10].y = 250;
	p[11].y = 150;

	p[12].y = 50;
	p[13].y = 150;
	p[14].y = 150;
	p[15].y = 50;
}

void				winctrl(t_main *a)
{
	int				run;
	t_vector		v1;
	t_vector		v2;
	t_vector		v3;

	v1.x = 250;
	v1.y = 250;
	v2.x = 150;
	v2.y = 250;
	v3.x = 250;
	v3.y = 00;

	point			p[16];

	mkpoint(p);
	run = 1;
	while (run)
	{
		while (SDL_PollEvent(&a->event))
		{
			if (a->event.type == SDL_QUIT)
				run = 0;
			}
		//surface(a, p);
		triangle(a, v1, v2, v3);
		refresh(a);
		SDL_BlitSurface(a->img, NULL, a->canvis, NULL);
		SDL_UpdateWindowSurface(a->win);
	}
	SDL_FreeSurface(a->img);
	SDL_FreeSurface(a->canvis);
	SDL_DestroyWindow(a->win);
	SDL_Quit();
}

int					main(int ac, char **av)
{
	static t_main			a;

	SDL_Init(SDL_INIT_EVERYTHING);
	a.win = SDL_CreateWindow("MOD 1 - Project", SDL_WINDOWPOS_UNDEFINED,
	SDL_WINDOWPOS_UNDEFINED, SCREENX, SCREENY,
	SDL_WINDOW_OPENGL);
	a.canvis = SDL_GetWindowSurface(a.win);

	winctrl(&a);
}
