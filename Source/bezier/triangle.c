#include "mod.h"

int cross_product(t_vector v1, t_vector v2)
{
	return (v1.x * v2.y - v1.y * v2.x);
}

void		triangle(t_main *a, t_vector v1, t_vector v2, t_vector v3)
{
	int			maxx;
	int			minx;
	int			maxy;
	int			miny;
	t_vector	vs1;
	t_vector	vs2;
	t_vector	q;
	float		s;
	float		t;
	int			x;
	int			y;

	maxx = ft_max(v1.x, ft_max(v2.x, v3.x));
	minx = ft_min(v1.x, ft_min(v2.x, v3.x));
	maxy = ft_max(v1.y, ft_max(v2.y, v3.y));
	miny = ft_min(v1.y, ft_min(v2.y, v3.y));
	vs1.x = v2.x - v1.x;
	vs1.y = v2.y - v1.y;
	vs2.x = v3.x - v1.x;
	vs2.y = v3.y - v1.y;
	x = maxx - 1;
	while (++x <= minx)
	{
		y = maxy - 1;
		while (++y <= miny)
		{
			q.x = x - v1.x;
			q.y = y - v1.y;
			s = (float)cross_product(q, vs2) / cross_product(vs1, vs2);
			t = (float)cross_product(vs1, q) / cross_product(vs1, vs2);
			if ((s >= 0) && (t >= 0) && (s + t <= 1))
				put_pixel(a, x, y, 255);
		}
	}
}