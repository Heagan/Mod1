#include "mod.h"

void		square(t_main *a, t_vector v1, t_vector v2)
{
	float	x;
	float	y;
	float	indent;

	y = v1.y - 1;
	while (++y < v2.y)
	{
		x = v1.x - 1;
		while (++x < v2.x)
			put_pixel(a, x, y, 255);
	}
}