/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_wrap.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gsferopo <gsferopo@42.fr>                  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/07/17 12:59:58 by gsferopo          #+#    #+#             */
/*   Updated: 2017/10/18 16:18:41 by gsferopo         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

double			ft_wrap(double min, double max, double n)
{
	double		b;

	if (min > max)
	{
		b = min;
		min = max;
		max = b;
	}
	b = 1;
	while (b)
	{
		if (n > max)
			n = min + (n - max - 1);
		else if (n < min)
			n = max - (n + min + 1);
		else
			b = 0;
	}
	return (n);
}
